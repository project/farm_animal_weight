<?php

namespace Drupal\farm_animal_weight;

use Drupal\asset\Entity\AssetInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\farm_log\LogQueryFactoryInterface;
use Drupal\quantity\Entity\QuantityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Animal weight logic.
 */
class AnimalWeight implements AnimalWeightInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Log query factory.
   *
   * @var \Drupal\farm_log\LogQueryFactoryInterface
   */
  protected LogQueryFactoryInterface $logQueryFactory;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\farm_log\LogQueryFactoryInterface $log_query_factory
   *   Log query factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LogQueryFactoryInterface $log_query_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logQueryFactory = $log_query_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('farm.log_query'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function hasWeight(AssetInterface $asset): bool {
    $count = $this->entityTypeManager->getStorage('log')->getAggregateQuery()
      ->accessCheck(TRUE)
      ->condition('type', 'observation')
      ->condition('status', 'done')
      ->condition('asset', $asset->id())
      ->condition('quantity.entity.measure', 'weight')
      ->condition('quantity.entity.value__denominator', NULL, 'IS NOT NULL')
      ->count()
      ->execute();
    return $count > 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentWeight(AssetInterface $asset): ?QuantityInterface {

    // Get the most recent weight.
    $weights = $this->getWeights($asset, 1);

    // Bail if there are no weights.
    if (empty($weights) || !isset($weights[0]['quantity'])) {
      return NULL;
    }

    // Return the quantity of the first weight.
    return $weights[0]['quantity'];
  }

  /**
   * {@inheritdoc}
   */
  public function getWeights(AssetInterface $asset, int $limit = NULL): array {

    // Start an array of weights.
    $weights = [];

    // Get weight logs.
    $logs = $this->getWeightLogs($asset, $limit);
    if (empty($logs)) {
      return $weights;
    }

    // Get the weight quantity from each log.
    foreach ($logs as $log) {

      // Build a weight with the first weight quantity.
      $weight = ['log' => $log];
      foreach ($log->get('quantity')->referencedEntities() as $quantity) {
        if ($quantity->get('measure')->value === 'weight' && !$quantity->get('value')->isEmpty()) {
          $weight['quantity'] = $quantity;
          break;
        }
      }

      // Only append the weight if a weight quantity was found.
      if (isset($weight['quantity'])) {
        $weights[] = $weight;
      }
    }

    return $weights;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeightLogs(AssetInterface $asset, int $limit = NULL): array {
    $options = [
      'type' => 'observation',
      'asset' => $asset,
      'status' => 'done',
      'limit' => $limit,
    ];
    $query = $this->logQueryFactory->getQuery($options)
      ->accessCheck(TRUE)
      ->condition('quantity.entity.measure', 'weight')
      ->condition('quantity.entity.value__denominator', NULL, 'IS NOT NULL');

    $log_ids = $query->execute();
    if (empty($log_ids)) {
      return [];
    }

    return $this->entityTypeManager->getStorage('log')->loadMultiple($log_ids);
  }

}

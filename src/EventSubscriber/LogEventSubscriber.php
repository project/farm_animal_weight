<?php

namespace Drupal\farm_animal_weight\EventSubscriber;

use Drupal\asset\Entity\AssetInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\log\Entity\LogInterface;
use Drupal\log\Event\LogEvent;
use Drupal\quantity\Entity\QuantityInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Invalidate asset cache when current weight changes.
 */
class LogEventSubscriber implements EventSubscriberInterface {

  /**
   * Cache tag invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected CacheTagsInvalidatorInterface $cacheTagsInvalidator;

  /**
   * Datetime time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * LogEventSubscriber Constructor.
   *
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   Cache tag invalidator service.
   * @param \Drupal\Component\Datetime\TimeInterface $date_time
   *   Datetime time service.
   */
  public function __construct(CacheTagsInvalidatorInterface $cache_tags_invalidator, TimeInterface $date_time) {
    $this->time = $date_time;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      LogEvent::DELETE => 'logDelete',
      LogEvent::PRESAVE => 'logPresave',
    ];
  }

  /**
   * Perform actions on log delete.
   *
   * @param \Drupal\log\Event\LogEvent $event
   *   The log event.
   */
  public function logDelete(LogEvent $event) {
    $this->invalidateAssetCacheOnWeightChange($event->log);
  }

  /**
   * Perform actions on log presave.
   *
   * @param \Drupal\log\Event\LogEvent $event
   *   The log event.
   */
  public function logPresave(LogEvent $event) {
    $this->invalidateAssetCacheOnWeightChange($event->log);
  }

  /**
   * Invalidate asset caches when assets weight changes.
   *
   * @param \Drupal\log\Entity\LogInterface $log
   *   The Log entity.
   */
  protected function invalidateAssetCacheOnWeightChange(LogInterface $log): void {

    // Keep track if we need to invalidate the cache.
    $update_asset_cache = FALSE;

    // If the log is an active quantity measurement, invalidate the cache.
    if ($this->isActiveWeightLog($log)) {
      $update_asset_cache = TRUE;
    }

    // If updating an existing log, invalidate the cache.
    // This catches logs changing from done to pending.
    if (!empty($log->original) && $this->isActiveWeightLog($log->original)) {
      $update_asset_cache = TRUE;
    }

    // If an update is not necessary, bail.
    if (!$update_asset_cache) {
      return;
    }

    // Build a list of cache tags.
    $tags = [];

    // Include assets that were previously referenced by the log.
    if (!empty($log->original)) {
      array_push($tags, ...$this->getWeightAssetCacheTags($log->original));
    }

    // Include assets currently referenced by the log.
    array_push($tags, ...$this->getWeightAssetCacheTags($log));

    // Invalidate the cache tags.
    $this->cacheTagsInvalidator->invalidateTags($tags);
  }

  /**
   * Helper function to determine if a log is active and has a weight quantity.
   *
   * @param \Drupal\log\Entity\LogInterface $log
   *   The log to check.
   *
   * @return bool
   *   Boolean indicating if the log is active.
   */
  protected function isActiveWeightLog(LogInterface $log): bool {

    // Check if the log is active and has a quantity.
    $active_quantity = $log->bundle() === 'observation' && $log->get('status')->value == 'done' && $log->get('timestamp')->value <= $this->time->getCurrentTime() && !$log->get('quantity')->isEmpty();

    // Check that there is a weight quantity with a value.
    $weight_quantity = array_filter($log->get('quantity')->referencedEntities(), function (QuantityInterface $quantity) {
      return $quantity->get('measure')->value === 'weight' && !$quantity->get('value')->isEmpty();
    });

    return $active_quantity && count($weight_quantity);
  }

  /**
   * Helper function to load asset cache tags from the log.asset field.
   *
   * @param \Drupal\log\Entity\LogInterface $log
   *   The log to check.
   *
   * @return string[]
   *   An array of cache tags.
   */
  protected function getWeightAssetCacheTags(LogInterface $log): array {

    // Filter to only log quantities with an inventory adjustment.
    $assets = array_filter($log->get('asset')->referencedEntities(), function (AssetInterface $asset) {
      return $asset->bundle() === 'animal';
    });

    // Return asset cache tags.
    $cache_tags = array_map(function (AssetInterface $asset) {
      return $asset->getCacheTags();
    }, $assets);

    return array_merge(...$cache_tags);
  }

}

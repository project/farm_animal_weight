<?php

namespace Drupal\farm_animal_weight\Access;

use Drupal\asset\Entity\Asset;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\farm_animal_weight\AnimalWeightInterface;

/**
 * Checks access for displaying the animal weight view.
 */
class FarmAssetAnimalWeightViewsAccessCheck implements AccessInterface {

  /**
   * The animal weight service.
   *
   * @var \Drupal\farm_animal_weight\AnimalWeightInterface
   */
  protected $animalWeight;

  /**
   * FarmAssetAnimalWeightViewsAccessCheck constructor.
   *
   * @param \Drupal\farm_animal_weight\AnimalWeightInterface $animal_weight
   *   The animal weight service.
   */
  public function __construct(AnimalWeightInterface $animal_weight) {
    $this->animalWeight = $animal_weight;
  }

  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function access(RouteMatchInterface $route_match) {

    // If there is no "asset" parameter, bail.
    $asset_id = $route_match->getParameter('asset');
    if (empty($asset_id)) {
      return AccessResult::forbidden();
    }

    // If the asset does not exist or is not an animal, bail.
    $asset = Asset::load($asset_id);
    if (empty($asset) || $asset->bundle() !== 'animal') {
      return AccessResult::forbidden();
    }

    // Check if the asset has a weight.
    $has_weight = $this->animalWeight->hasWeight($asset);
    $access = AccessResult::allowedIf($has_weight);

    // Invalidate the access result when the asset is updated.
    $access->addCacheTags($asset->getCacheTags());
    return $access;
  }

}

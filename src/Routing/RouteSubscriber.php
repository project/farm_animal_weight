<?php

namespace Drupal\farm_animal_weight\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alter routes for the farm_animal_weight module.
 *
 * @ingroup farm
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {

    // Add our _asset_animal_weight_access requirement to
    // view.farm_animal_weight.page.
    if ($route = $collection->get('view.farm_animal_weight.page')) {
      $route->setRequirement('_asset_animal_weight_access', 'Drupal\farm_animal_weight\Access\FarmAssetAnimalWeightViewsAccessCheck::access');
    }
  }

}

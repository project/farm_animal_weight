<?php

namespace Drupal\farm_animal_weight\Form;

use Drupal\asset\Entity\Asset;
use Drupal\asset\Entity\AssetInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\farm_animal_weight\AnimalWeightInterface;
use Drupal\farm_group\GroupMembershipInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Animal weight report form.
 */
class AnimalWeightReport extends FormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The group membership service.
   *
   * @var \Drupal\farm_group\GroupMembershipInterface
   */
  protected $groupMembership;

  /**
   * The animal weight service.
   *
   * @var \Drupal\farm_animal_weight\AnimalWeightInterface
   */
  protected $animalWeight;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'farm_animal_weight_animal_weight_report';
  }

  /**
   * Constructs a new AnimalWeightReport form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\farm_group\GroupMembershipInterface $group_membership
   *   The group membership service.
   * @param \Drupal\farm_animal_weight\AnimalWeightInterface $animal_weight
   *   The animal weight service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, GroupMembershipInterface $group_membership, AnimalWeightInterface $animal_weight) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->groupMembership = $group_membership;
    $this->animalWeight = $animal_weight;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('group.membership'),
      $container->get('farm.animal_weight'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\asset\AssetStorage $asset_storage */
    $asset_storage = $this->entityTypeManager->getStorage('asset');
    $date_format = 'Y-m-d';

    // Build an input fieldset.
    $form['input'] = [
      '#type' => 'details',
      '#title' => $this->t('Input form'),
      '#open' => TRUE,
    ];

    // Build list of group assets.
    $animal_groups = $asset_storage->loadByProperties(['type' => 'group']);
    $animal_group_options = array_map(function (AssetInterface $asset) {
      $label = $asset->label();
      if ($asset->get('archived')->value && $time = $asset->getArchivedTime()) {
        $date = date('Y-m-d', $asset->getArchivedTime());
        $label = $label . ' (' . $this->t('archived') . ' ' . $date . ')';
      }
      return $label;
    }, $animal_groups);

    // Group selection.
    $form['input']['group'] = [
      '#type' => 'select',
      '#title' => $this->t('Animal group'),
      '#description' => $this->t('Select the group(s) of animals to include in the weight report.'),
      '#options' => $animal_group_options,
      '#required' => TRUE,
      '#multiple' => TRUE,
    ];

    // Archived filter.
    $form['input']['archived'] = [
      '#type' => 'select',
      '#title' => $this->t('Archived'),
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
        2 => '-' . $this->t('Any') . '-',
      ],
    ];

    // Build flag options.
    // @todo Change this to use a flag service function.
    $flag_options = [];
    $base_field_definitions = $this->entityFieldManager->getFieldStorageDefinitions('asset');
    if (!empty($base_field_definitions['flag'])) {
      $animal = Asset::create([
        'type' => 'animal',
      ]);
      $flag_options = farm_flag_field_allowed_values($base_field_definitions['flag'], $animal);
    }

    // Flags filter.
    $form['input']['flags'] = [
      '#type' => 'select',
      '#title' => $this->t('Flags'),
      '#options' => $flag_options,
      '#multiple' => TRUE,
    ];

    // Birthdate filter.
    $form['input']['birth'] = [
      '#type' => 'details',
      '#title' => $this->t('Birthdate'),
      '#open' => FALSE,
    ];
    $form['input']['birth']['earliest_birth'] = [
      '#type' => 'date',
      '#title' => $this->t('Earliest birth'),
      '#default_value' => '',
      '#date_year_range' => '-10:+1',
      '#date_format' => $date_format,
      '#date_label_position' => 'within',
    ];
    $form['input']['birth']['latest_birth'] = [
      '#type' => 'date',
      '#title' => $this->t('Latest birth'),
      '#default_value' => '',
      '#date_year_range' => '-10:+1',
      '#date_format' => $date_format,
      '#date_label_position' => 'within',
    ];

    // Log timestamp filters.
    $form['input']['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start date'),
      '#description' => $this->t('First recorded date of animal weights to include.'),
      '#default_value' => '',
      '#date_year_range' => '-10:+1',
      '#date_format' => $date_format,
      '#date_label_position' => 'within',
    ];
    $form['input']['end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End date'),
      '#description' => $this->t('Last recorded date of animal weights to include.'),
      '#default_value' => '',
      '#date_year_range' => '-10:+1',
      '#date_format' => $date_format,
      '#date_label_position' => 'within',
    ];

    // Submit button.
    $form['input']['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => [$this, 'returnReport'],
        'wrapper' => 'weight-report-results',
      ],
    ];

    // Results container.
    $form['results'] = [
      '#type' => 'container',
      '#prefix' => '<div id="weight-report-results">',
      '#suffix' => '</div>',
    ];

    // If a group was submitted, build the report.
    if (!empty($form_state->hasValue('group'))) {
      $form['results']['report'] = $this->buildReport($form, $form_state);
    }

    return $form;
  }

  /**
   * AJAX callback to return the report.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The report render array.
   */
  public function returnReport(array $form, FormStateInterface $form_state) {
    return $form['results'];
  }

  /**
   * Helper function to build the weight report.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The results fieldset render array.
   */
  private function buildReport(array &$form, FormStateInterface $form_state) {

    /** @var \Drupal\asset\AssetStorage $asset_storage */
    $asset_storage = $this->entityTypeManager->getStorage('asset');
    $date_format = 'Y-m-d';
    $colors = [
      '#589ac5', # Gin info blue
      '#058260', # Gin green
      '#d83f0c', # Gin primary red
      '#efcf64', # Gin warning yellow
    ];

    // Groups.
    $group_ids = $form_state->getValue('group');

    // Archived.
    $include_archived = (bool) $form_state->getValue('archived');
    $only_archived = (int) $form_state->getValue('archived') === 1;

    // Flags.
    $flag_ids = $form_state->getValue('flags');

    // Birthdate.
    $earliest_birth = strtotime($form_state->getValue('earliest_birth'));
    $latest_birth = strtotime($form_state->getValue('latest_birth'));

    // Log timestamp.
    $start_date = strtotime($form_state->getValue('start_date'));
    $end_date = strtotime($form_state->getValue('end_date'));

    // Build average daily weights for each group.
    // This will contain an array of timestamps keyed by group ids.
    // Each timestamp will specify the units, total_weight and animal_count.
    $group_date_averages = [];

    // Collect data for each group.
    $group_assets = $asset_storage->loadMultiple($group_ids);
    foreach ($group_assets as $group) {

      // Collect data for each group member.
      $members = $this->groupMembership->getGroupMembers([$group]);
      foreach ($members as $member) {

        // Check if the asset is an animal.
        if ($member->bundle() !== 'animal') {
          continue;
        }

        // Check if the archived state matches the filter.
        $archived = $member->get('archived')->value;
        if (($only_archived && !$archived) || (!$include_archived && $archived)) {
          continue;
        }

        // Check if the asset has required flags.
        if (!empty($flag_ids)) {
          $diff = array_diff($flag_ids, $member->get('flag')->value);
          if (!empty($diff)) {
            continue;
          }
        }

        // Check if the birthdate matches the filter.
        if (!$member->get('birthdate')->isEmpty() && ($earliest_birth || $latest_birth)) {
          $birthdate = $member->get('birthdate')->value;

          if ($earliest_birth && $birthdate < $earliest_birth) {
            continue;
          }

          if ($latest_birth && $birthdate > $latest_birth) {
            continue;
          }
        }

        // Get all weights for the animal.
        $weights = $this->animalWeight->getWeights($member);
        foreach ($weights as $weight) {

          /** @var \Drupal\log\Entity\LogInterface $log */
          $log = $weight['log'];
          $log_timestamp = $log->get('timestamp')->value;

          // Check if the log timestamp matches the filter.
          if ($start_date && $log_timestamp < $start_date) {
            continue;
          }
          if ($end_date && $log_timestamp > $end_date) {
            continue;
          }

          // Get the timestamp for the log's date.
          $log_date_timestamp = $log->get('timestamp')->value * 1000;

          // Get the weight quantity units and value.
          /** @var \Drupal\quantity\Entity\QuantityInterface $quantity */
          $quantity = $weight['quantity'];
          $units = NULL;
          if (!$quantity->get('units')->isEmpty()) {
            $units = $quantity->get('units')->entity->label();
          }
          $value = (float) $quantity->get('value')->decimal;

          // Get the animal inventory.
          // @todo Use the member inventory.
          $inventory = 1;

          // Add the value to the group date averages.
          // Initialize the group date.
          if (!isset($group_date_averages[$group->id()]['dates'][$log_date_timestamp])) {
            $group_date_averages[$group->id()]['dates'][$log_date_timestamp] = [
              'units' => $units,
              'total_weight' => $value,
              'animal_count' => $inventory,
            ];
          }
          // Else increment the existing group date values.
          else {
            $group_date_averages[$group->id()]['dates'][$log_date_timestamp]['total_weight'] += $value;
            $group_date_averages[$group->id()]['dates'][$log_date_timestamp]['animal_count'] += $inventory;
          }
        }
      }
    }

    // Bail if no data was found.
    if (empty($group_date_averages)) {
      return [
        '#markup' => $this->t('No weights found.'),
      ];
    }

    // Build chart definition.
    $chart = [
      '#type' => 'chart',
      '#chart_type' => 'line',
      '#title' => $this->t('Animal weight report'),
      '#title_font_weight' => 'bold',
      '#title_font_size' => '16',
    ];

    // Build chart axes.
    $chart['x_axis'] = [
      '#type' => 'chart_xaxis',
      '#axis_type' => 'time',
      '#title' => $this->t('Date'),
      '#title_font_weight' => 'bold',
      '#title_font_size' => '14',
      '#labels_rotation' => 20,
    ];
    $chart['y_axis'] = [
      '#type' => 'chart_yaxis',
      '#title' => $this->t('Average weight'),
      '#title_font_weight' => 'bold',
      '#title_font_size' => '14',
    ];

    // Add units if available.
    if (!empty($units)) {
      $chart['y_axis']['#title'] .= " ($units) ";
    }

    // Create dataset for each group's average data.
    $group_averages_keys = array_keys($group_date_averages);
    foreach ($group_date_averages as $group_id => $averages) {
      $group = Asset::load($group_id);

      // Sort the logs by date.
      ksort($averages['dates']);

      // Build x/y data pairs.
      $group_data = [];
      foreach ($averages['dates'] as $date => $data) {
        $group_data[] = ['x' => $date, 'y' => $data['total_weight'] / $data['animal_count']];
      }

      // Determine color.
      $group_index = array_search($group_id, $group_averages_keys);
      $color = $colors[ $group_index % count($colors)];

      // Add dataset.
      $chart["group_$group_id"] = [
        '#type' => 'chart_data',
        '#title' => $group->label(),
        '#data' => $group_data,
        '#color' => $color,
      ];
    }

    return $chart;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}

<?php

namespace Drupal\farm_animal_weight\Field;

use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;

/**
 * Computes the current weight for animal assets.
 */
class AnimalCurrentWeightItemList extends EntityReferenceRevisionsFieldItemList {

  use ComputedItemListTrait;

  /**
   * Computes the current weight for animal assets.
   */
  protected function computeValue() {

    // Get the asset entity.
    $entity = $this->getEntity();

    // Get the asset's current weight.
    /** @var \Drupal\farm_animal_weight\AnimalWeightInterface $animal_weight */
    $animal_weight = \Drupal::service('farm.animal_weight');
    $weight = $animal_weight->getCurrentWeight($entity);

    if (empty($weight)) {
      return;
    }

    $this->list[0] = $this->createItem(0, $weight);
  }

}

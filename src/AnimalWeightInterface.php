<?php

namespace Drupal\farm_animal_weight;

use Drupal\asset\Entity\AssetInterface;
use Drupal\quantity\Entity\QuantityInterface;

/**
 * Animal weight logic.
 */
interface AnimalWeightInterface {

  /**
   * Check if an asset has a weight observation.
   *
   * @param \Drupal\asset\Entity\AssetInterface $asset
   *   The asset.
   *
   * @return bool
   *   Boolean indicating if the asset has a weight.
   */
  public function hasWeight(AssetInterface $asset): bool;

  /**
   * Get the current weight of an asset.
   *
   * @param \Drupal\asset\Entity\AssetInterface $asset
   *   The asset.
   *
   * @return \Drupal\quantity\Entity\QuantityInterface|null
   *   The weight quantity or NULL if the asset does not have a weight.
   */
  public function getCurrentWeight(AssetInterface $asset): ?QuantityInterface;

  /**
   * Get the latest observed weights of an asset.
   *
   * @param \Drupal\asset\Entity\AssetInterface $asset
   *   The asset.
   * @param int $limit
   *   The number of latest weights to return. Defaults to NULL, returning all.
   *
   * @return array
   *   An array of arrays containing the observation log and quantity.
   */
  public function getWeights(AssetInterface $asset, int $limit = NULL): array;

  /**
   * Get the latest weight observation logs that reference an asset.
   *
   * @param \Drupal\asset\Entity\AssetInterface $asset
   *   The asset.
   * @param int $limit
   *   The number of latest weight logs to return.
   *
   * @return \Drupal\log\Entity\LogInterface[]
   *   The weight logs.
   */
  public function getWeightLogs(AssetInterface $asset, int $limit): array;

}

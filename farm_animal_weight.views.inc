<?php

/**
 * @file
 * Provides Views data for farm_animal_weight.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function farm_animal_weight_views_data_alter(array &$data) {

  // Add computed fields to animal assets.
  if (isset($data['asset'])) {

    // Computed geometry.
    $data['asset']['current_weight_target_id'] = [
      'title' => t('Current weight'),
      'field' => [
        'id' => 'field',
        'field_name' => 'current_weight',
      ],
    ];
  }
}

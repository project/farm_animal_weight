<?php

namespace Drupal\Tests\farm_animal_weight\Functional;

use Drupal\asset\Entity\Asset;
use Drupal\asset\Entity\AssetInterface;
use Drupal\fraction\Fraction;
use Drupal\log\Entity\Log;
use Drupal\quantity\Entity\Quantity;
use Drupal\quantity\Entity\QuantityInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\farm_test\Functional\FarmBrowserTestBase;
use Drupal\Tests\farm_test\Kernel\FarmEntityCacheTestTrait;

/**
 * Tests for animal weight logic.
 *
 * @group farm
 */
class AnimalWeightTest extends FarmBrowserTestBase {

  use FarmEntityCacheTestTrait;

  /**
   * The animal weight service.
   *
   * @var \Drupal\farm_animal_weight\AnimalWeightInterface
   */
  protected $animalWeight;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'farm_animal_weight',
    'farm_quantity_standard',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Get the animal weight service.
    $this->animalWeight = $this->container->get('farm.animal_weight');
  }

  /**
   * Test the animal weight service.
   */
  public function testAnimalWeightService() {

    // Create a lbs unit.
    /** @var \Drupal\taxonomy\TermInterface $cow */
    $lbs = Term::create([
      'name' => 'lbs',
      'vid' => 'unit',
    ]);
    $lbs->save();

    // Create a Cow animal type term.
    /** @var \Drupal\taxonomy\TermInterface $cow */
    $cow = Term::create([
      'name' => 'Cow',
      'vid' => 'animal_type',
    ]);
    $cow->save();

    // Create an asset.
    /** @var \Drupal\asset\Entity\AssetInterface $animal */
    $asset = Asset::create([
      'name' => $this->randomMachineName(),
      'type' => 'animal',
      'animal_type' => ['tid' => $cow->id()],
      'status' => 'active',
    ]);
    $asset->save();
    $this->populateEntityTestCache($asset);

    // Assert that the asset has no weight.
    $this->assertCorrectWeight($asset);

    // Create a pending observation log with a weight quantity.
    $fraction1 = Fraction::createFromDecimal(100);
    $quantity1 = Quantity::create([
      'type' => 'standard',
      'measure' => 'weight',
      'value' => [
        'numerator' => $fraction1->getNumerator(),
        'denominator' => $fraction1->getDenominator(),
      ],
      'units' => $lbs,
    ]);
    $quantity1->save();
    $log1 = Log::create([
      'type' => 'observation',
      'status' => 'pending',
      'timestamp' => \Drupal::time()->getRequestTime(),
      'asset' => $asset,
      'quantity' => $quantity1,
    ]);
    $log1->save();

    // Assert that the asset has no weight.
    $this->assertEntityTestCache($asset, TRUE);
    $this->assertCorrectWeight($asset);

    // Update the log so it is done.
    $log1->set('status', 'done');
    $log1->save();

    // Assert that the asset has the first weight.
    $this->assertEntityTestCache($asset, FALSE);
    $this->assertCorrectWeight($asset, $quantity1, 1);

    // Create a second observation log with a weight quantity.
    $this->populateEntityTestCache($asset);
    $fraction2 = Fraction::createFromDecimal(100);
    $quantity2 = Quantity::create([
      'type' => 'standard',
      'measure' => 'weight',
      'value' => [
        'numerator' => $fraction2->getNumerator(),
        'denominator' => $fraction2->getDenominator(),
      ],
      'units' => $lbs,
    ]);
    $quantity2->save();
    $log2 = Log::create([
      'type' => 'observation',
      'status' => 'done',
      'timestamp' => \Drupal::time()->getRequestTime(),
      'asset' => $asset,
      'quantity' => $quantity2,
    ]);
    $log2->save();

    // Assert that the asset has the second weight.
    $this->assertEntityTestCache($asset, FALSE);
    $this->assertCorrectWeight($asset, $quantity2, 2);

    // Delete the second log.
    $this->populateEntityTestCache($asset);
    $log2->delete();

    // Assert that the asset has the first weight.
    $this->assertEntityTestCache($asset, FALSE);
    $this->assertCorrectWeight($asset, $quantity1, 1);
  }

  /**
   * Helper function to assert the correct animal weight is returned.
   *
   * @param \Drupal\asset\Entity\AssetInterface $asset
   *   The asset.
   * @param \Drupal\quantity\Entity\QuantityInterface|null $current_weight
   *   The current weight.
   * @param int $count
   *   The number of observed weights.
   */
  protected function assertCorrectWeight(AssetInterface $asset, QuantityInterface $current_weight = NULL, int $count = 0) {

    // Assert the animal's current weight.
    if (empty($current_weight)) {
      $this->assertFalse($this->animalWeight->hasWeight($asset), 'The animal has no weight.');
      $this->assertNull($this->animalWeight->getCurrentWeight($asset), 'The animal has no current weight.');
    }
    else {
      $this->assertTrue($this->animalWeight->hasWeight($asset), 'The animal has a weight.');
      $actual_weight = $this->animalWeight->getCurrentWeight($asset);
      $this->assertNotNull($actual_weight, 'The animal has a current weight.');
      $this->assertEquals($current_weight->id(), $actual_weight->id(), 'The animal has the correct current weight.');
    }

    // Assert the correct number of weight logs.
    $this->assertEquals($count, count($this->animalWeight->getWeightLogs($asset)), 'The animal has the correct number of weight logs.');
    $this->assertEquals($count, count($this->animalWeight->getWeights($asset)), 'The animal has the correct number of observed weights.');
  }

}
